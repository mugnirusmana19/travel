<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title></title>

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="{{asset('porto/img/favicon.ico')}}" type="image/x-icon" />
		<link rel="apple-touch-icon" href="{{asset('porto/img/apple-touch-icon.png')}}">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Web Fonts  -->
		<link href="{{asset('http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light')}}" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{asset('porto/vendor/bootstrap/css/bootstrap.css')}}">
		<link rel="stylesheet" href="{{asset('porto/vendor/font-awesome/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('porto/vendor/simple-line-icons/css/simple-line-icons.css')}}">
		<link rel="stylesheet" href="{{asset('porto/vendor/owl.carousel/assets/owl.carousel.min.css')}}">
		<link rel="stylesheet" href="{{asset('porto/vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
		<link rel="stylesheet" href="{{asset('porto/vendor/magnific-popup/magnific-popup.css')}}">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{asset('porto/css/style.css')}}">
		<link rel="stylesheet" href="{{asset('porto/css/theme.css')}}">
		<link rel="stylesheet" href="{{asset('porto/css/theme-elements.css')}}">
		<link rel="stylesheet" href="{{asset('porto/css/theme-blog.css')}}">
		<link rel="stylesheet" href="{{asset('porto/css/theme-shop.css')}}">
		<link rel="stylesheet" href="{{asset('porto/css/theme-animate.css')}}">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="{{asset('porto/vendor/rs-plugin/css/settings.css')}}" media="screen">
		<link rel="stylesheet" href="{{asset('porto/vendor/rs-plugin/css/layers.css')}}" media="screen">
		<link rel="stylesheet" href="{{asset('porto/vendor/rs-plugin/css/navigation.css')}}" media="screen">
		<link rel="stylesheet" href="{{asset('porto/vendor/circle-flip-slideshow/css/component.css')}}" media="screen">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="{{asset('porto/css/skins/default.css')}}">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{asset('porto/css/custom.css')}}">

		<!-- Head Libs -->
		<script src="{{asset('porto/vendor/modernizr/modernizr.js')}}"></script>

	</head>
	<body>
		<div class="body">
			<header id="header" data-plugin-options='{"stickyEnabled": true, "stickyEnableOnBoxed": true, "stickyEnableOnMobile": true, "stickyStartAt": 57, "stickySetTop": "-57px", "stickyChangeLogo": true}'>
				<div class="header-body">
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-logo">
									<a href="/">
										<img alt="Porto" width="111" height="54" data-sticky-width="82" data-sticky-height="40" data-sticky-top="33" src="{{asset('porto/img/icon.png')}}">
									</a>
								</div>
							</div>
							<div class="header-column">
								<div class="header-row">
									<div class="header-search hidden-xs">
										<form id="searchForm" action="" method="get">
											<div class="input-group">
												<input type="text" class="form-control" name="id" id="id" placeholder="Search..." required>
												<span class="input-group-btn">
													<button class="btn btn-default" type="submit"><i class="fa fa-search" style="margin: 4px 5px 5px 5px"></i></button>
												</span>
											</div>
										</form>
									</div>
									<nav class="header-nav-top">
										<ul class="nav nav-pills">
											<li class="hidden-xs">
												<a href=""><i class="fa fa-angle-right"></i> About Us</a>
											</li>
											<li class="hidden-xs">
												<a href=""><i class="fa fa-angle-right"></i> Contact Us</a>
											</li>
											<li>
												<span class="ws-nowrap"><i class="fa fa-phone"></i> (123) 456-789</span>
											</li>
										</ul>
									</nav>
								</div>
								<div class="header-row">
									<div class="header-nav">
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
											<i class="fa fa-bars"></i>
										</button>
										<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
											<nav>
												<ul class="nav nav-pills" id="mainNav">
													<li>
														<a href="/">
															Home
														</a>
													</li>
													<li>
                            <a href="/object_wisata">
															objek wisata
														</a>
													</li>
													<li>
                            <a href="/tiket">
															Tiker Kereta Api
														</a>
													</li>
                          <li>
                            <a href="/rest_area">
															Rest Area
														</a>
													</li>
												</ul>
											</nav>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

      <!-Isi content-->
      @yield('content');

			<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="footer-ribbon">
							<span>Get in Touch</span>
						</div>
						<div class="col-md-6">
							<div class="newsletter">
								<h4>Tentang Kami</h4>
								<p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>

								<div class="alert alert-success hidden" id="newsletterSuccess">
									<strong>Success!</strong> You've been added to our email list.
								</div>

								<div class="alert alert-danger hidden" id="newsletterError"></div>

							</div>
						</div>
						<div class="col-md-4">
							<div class="contact-details">
								<h4>Hubungi Kami</h4>
								<ul class="contact">
									<li><p><i class="fa fa-map-marker"></i> <strong>Address:</strong> 1234 Street Name, City Name, United States</p></li>
									<li><p><i class="fa fa-phone"></i> <strong>Phone:</strong> (123) 456-789</p></li>
									<li><p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:mail@example.com">mail@example.com</a></p></li>
								</ul>
							</div>
						</div>
						<div class="col-md-2">
							<h4>Ikuti Kami</h4>
							<ul class="social-icons">
								<li class="social-icons-facebook"><a href="{{asset('http://www.facebook.com/')}}" target="_blank"><i class="fa fa-facebook" style="margin: 9px 5px 5px 5px"></i></a></li>
								<li class="social-icons-twitter"><a href="{{asset('http://www.twitter.com/')}}" target="_blank"><i class="fa fa-twitter" style="margin: 9px 5px 5px 5px"></i></a></li>
								<li class="social-icons-linkedin"><a href="{{asset('http://www.linkedin.com/')}}" target="_blank"><i class="fa fa-linkedin" style="margin: 9px 5px 5px 5px"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-md-1">
								<a href="index.html" class="logo">
									<img alt="Porto Website Template" class="img-responsive" src="{{asset('porto/img/logo-footer.png')}}">
								</a>
							</div>
							<div class="col-md-7">
								<p>&copy; Copyright 2017. All Rights Reserved.</p>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<script src="{{asset('porto/vendor/jquery/jquery.js')}}"></script>
		<script src="{{asset('porto/vendor/jquery.appear/jquery.appear.js')}}"></script>
		<script src="{{asset('porto/vendor/jquery.easing/jquery.easing.js')}}"></script>
		<script src="{{asset('porto/vendor/jquery-cookie/jquery-cookie.js')}}"></script>
		<script src="{{asset('porto/vendor/bootstrap/js/bootstrap.js')}}"></script>
		<script src="{{asset('porto/vendor/common/common.js')}}"></script>
		<script src="{{asset('porto/vendor/jquery.validation/jquery.validation.js')}}"></script>
		<script src="{{asset('porto/vendor/jquery.stellar/jquery.stellar.js')}}"></script>
    <script src="{{asset('porto/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js')}}"></script>
    <script src="{{asset('porto/vendor/jquery.gmap/jquery.gmap.js')}}"></script>
		<script src="{{asset('porto/vendor/jquery.lazyload/jquery.lazyload.js')}}"></script>
		<script src="{{asset('porto/vendor/isotope/jquery.isotope.js')}}"></script>
		<script src="{{asset('porto/vendor/owl.carousel/owl.carousel.js')}}"></script>
		<script src="{{asset('porto/vendor/magnific-popup/jquery.magnific-popup.js')}}"></script>
    <script src="{{asset('porto/vendor/vide/vide.js')}}"></script>

		<!-- Theme Base, Components and Settings -->
		<script src="{{asset('porto/js/theme.js')}}"></script>

		<!-- Current Page Vendor and Views -->
		<script src="{{asset('porto/vendor/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
		<script src="{{asset('porto/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
		<script src="{{asset('porto/vendor/circle-flip-slideshow/js/jquery.flipshow.js')}}"></script>
		<script src="{{asset('porto/js/views/view.home.js')}}"></script>

		<!-- Theme Custom -->
		<script src="{{asset('porto/js/custom.js')}}"></script>

		<!-- Theme Initialization Files -->
		<script src="{{asset('porto/js/theme.init.js')}}"></script>

		<script>
			$(document).ready(function(){
			    $('[data-toggle="tooltip"]').tooltip();
			});
		</script>

	</body>
</html>
