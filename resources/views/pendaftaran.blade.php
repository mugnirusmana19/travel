@extends('layouts/master')

@section('content')
<div class="body">

  <div role="main" class="main shop">
    <section class="page-header">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>Object Wisata</h1>
          </div>
        </div>
      </div>
    </section>
    <div class="container">

      <form id="contactForm" action="" method="GET">

        <div class="row" style="margin-bottom:20px">
          <div class="col-md-10">
            <input type="text" name="serch" placeholder="Cari object wisata" class="form-control">
          </div>
          <div class="col-md-2">
            <a href="" class="btn btn-primary btn-md btn-block"><span class="fa fa-search"></span> Search</a>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <center>
              <table border="0" width="100%" class="table">
                <tr>
                  <td>
                    <h3 style="margin-bottom:-1">Farm House</h3>
                    <p>Jl. Raya Lembang No.108, Gudangkahuripan, Lembang, Kabupaten Bandung Barat, Jawa Barat 40391, Indonesia</p>
                  </td>
                  <td width="30%" align="right">
                    <img src="{{asset('/porto/img/lembang.jpg')}}" width="200px" height="100px" alt="">
                  </td>
                </tr>
                <tr>
                  <td>
                    <h3 style="margin-bottom:-1">Curug Maribaya</h3>
                    <p>Kampung Cikondang, Lamajang, Pangalengan, Cibodas, Lembang, Kabupaten Bandung Barat, Jawa Barat 40391, Indonesia</p>
                  </td>
                  <td width="30%" align="right">
                    <img src="{{asset('/porto/img/curug.jpg')}}" width="200px" height="100px" alt="">
                  </td>
                </tr>
                <tr>
                  <td>
                    <h3 style="margin-bottom:-1">Situ Lengkong Panjalu</h3>
                    <p>Panjalu, Ciamis, Jawa Barat 46264, Indonesia</p>
                  </td>
                  <td width="30%" align="right">
                    <img src="{{asset('/porto/img/panjalu.jpg')}}" width="200px" height="100px" alt="">
                  </td>
                </tr>
              </table>
            </center>
            <ul class="pagination pagination-md pull-right">
									<li><a href="#">«</a></li>
									<li class="active"><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">»</a></li>
								</ul>
          </div>
        </div>

      </form>

    </div>
  </div>

</div>
@endsection
