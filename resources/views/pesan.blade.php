@extends('layouts/master')

@section('content')
<div class="body">

  <div role="main" class="main shop">
    <section class="page-header">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>Pesan Tiket</h1>
          </div>
        </div>
      </div>
    </section>
    <div class="container">

      <form id="contactForm" action="" method="POST">

        <div class="col-md-12">
          <div class="row">

            <div class="form-group">
              <div class="col-md-12">
                <label>Origin</label>
                <select class="form-control">
                  <option value="">Jakarta</option>
                  <option value="">Bandung</option>
                  <option value="">Yogyakarta</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-12">
                <label>Destination</label>
                <select class="form-control">
                  <option value="">Jakarta</option>
                  <option value="">Bandung</option>
                  <option value="">Yogyakarta</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-12">
                <label>Date</label>
                  @php
                    $date=date('d/m/Y');
                  @endphp
                  <input type="text" name="" class="form-control" value="{{$date}}">
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-12">
                <label>Amount</label>
                  <input type="text" name="" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-12">
                  <a href="/tiket" class="btn btn-md btn-primary">Pesan Tiket</a>
              </div>
            </div>


          </div>
        </div>

      </form>

    </div>
  </div>

</div>
@endsection
