@extends('layouts/master')

@section('content')
<div class="body">

  <div role="main" class="main shop">
    <section class="page-header">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>TIKET KERETA API</h1>
          </div>
        </div>
      </div>
    </section>
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <a href="/tiket/pesan" class="btn btn-primary">PESAN</a>
        </div>
      </div>

      <table class="table">
        <thead>
          <tr>
            <th width="1%"><center>No</center></th>
            <th width="45%">Nama Kereta API</th>
            <th width="40%">Tujuan</th>
            <th width=""><center>Harga Tiket</center></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><center>1</center></td>
            <td>Progo</td>
            <td>Jakarta-Yogyakarta</td>
            <td><center>140.000</center></td>
          </tr>
          <tr>
            <td><center>2</center></td>
            <td>Kutojaya Utara</td>
            <td>Jakarta-Kutoarjo</td>
            <td><center>125.000</center></td>
          </tr>
          <tr>
            <td><center>3</center></td>
            <td>Gaya Baru Malam Selatan</td>
            <td>Jakarta-Yogyakarta-Surabaya</td>
            <td><center>104.000</center></td>
          </tr>
          <tr>
            <td><center>4</center></td>
            <td>Logawa</td>
            <td>Purwokerto-Jember</td>
            <td><center>74.000</center></td>
          </tr>
        </tbody>
      </table>
      <ul class="pagination pagination-md pull-right">
            <li><a href="#">«</a></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">»</a></li>
          </ul>

    </div>
  </div>

</div>
@endsection
