@extends('layouts/master')

@section('content')
<div class="body">

  <div role="main" class="main shop">
    <section class="page-header">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>Rest Area</h1>
          </div>
        </div>
      </div>
    </section>
    <div class="container">

      <form id="contactForm" action="" method="GET">

        <div class="row" style="margin-bottom:20px">
          <div class="col-md-10">
            <input type="text" name="serch" placeholder="Cari rest area" class="form-control">
          </div>
          <div class="col-md-2">
            <a href="" class="btn btn-primary btn-md btn-block"><span class="fa fa-search"></span> Search</a>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <center>
              <table border="0" width="100%" class="table">
                <tr>
                  <td>
                    <h3 style="margin-bottom:-1">Rest Area Pasteur</h3>
                    <p>Sukaraja, Cicendo, Kota Bandung, Jawa Barat 40175, Indonesia</p>
                  </td>
                  <td width="30%" align="right">
                    <img src="{{asset('/porto/img/pasteur.jpg')}}" width="200px" height="100px" alt="">
                  </td>
                </tr>
                <tr>
                  <td>
                    <h3 style="margin-bottom:-1">Rest Area KM 88</h3>
                    <p>Jl. Tol Cipularang, Sukajaya, Sukatani, Kabupaten Purwakarta, Jawa Barat 41167, Indonesia</p>
                  </td>
                  <td width="30%" align="right">
                    <img src="{{asset('/porto/img/km88.jpg')}}" width="200px" height="100px" alt="">
                  </td>
                </tr>
                <tr>
                  <td>
                    <h3 style="margin-bottom:-1">Rest Area 72 B Purwakarta</h3>
                    <p>Jl. Tol Cipularang No.72, Cigelam, Babakancikao, Kabupaten Purwakarta, Jawa Barat 41151, Indonesia</p>
                  </td>
                  <td width="30%" align="right">
                    <img src="{{asset('/porto/img/purwakarta.jpg')}}" width="200px" height="100px" alt="">
                  </td>
                </tr>
              </table>
            </center>
            <ul class="pagination pagination-md pull-right">
									<li><a href="#">«</a></li>
									<li class="active"><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">»</a></li>
								</ul>
          </div>
        </div>

      </form>

    </div>
  </div>

</div>
@endsection
