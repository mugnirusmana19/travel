@extends('layouts/master')

@section('content')
  <div role="main" class="main">

    <div class="slider-container rev_slider_wrapper" style="height: 700px;">

					<div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"gridwidth": 800, "gridheight": 700}'>
						<ul>
							<li data-transition="fade">
								<img src="{{asset('porto/img/foto1.jpg')}}"
									alt=""
									data-bgposition="center center"
									data-bgfit="cover"
									data-bgrepeat="no-repeat"
									class="rev-slidebg">

								<div class="tp-caption"
									data-x="center" data-hoffset="-150"
									data-y="center" data-voffset="-95"
									data-start="1000"
									style="z-index: 5"
									data-transform_in="x:[-300%];opacity:0;s:500;"><img src="{{asset('porto/img/slides/slide-title-border.png')}}" alt=""></div>

								<div class="tp-caption top-label"
									data-x="center" data-hoffset="0"
									data-y="center" data-voffset="-95"
									data-start="500"
									style="z-index: 5"
									data-transform_in="y:[-300%];opacity:0;s:500;">Kini telah hadi</div>

								<div class="tp-caption"
									data-x="center" data-hoffset="150"
									data-y="center" data-voffset="-95"
									data-start="1000"
									style="z-index: 5"
									data-transform_in="x:[300%];opacity:0;s:500;"><img src="{{asset('porto/img/slides/slide-title-border.png')}}" alt=""></div>

								<div class="tp-caption main-label"
									data-x="center" data-hoffset="0"
									data-y="center" data-voffset="-45"
									data-start="1500"
									data-whitespace="nowrap"
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">Travel Online</div>

								<div class="tp-caption bottom-label"
									data-x="center" data-hoffset="0"
									data-y="center" data-voffset="5"
									data-start="2000"
									style="z-index: 5"
									data-transform_in="y:[100%];opacity:0;s:500;">Lebih mudah dan lebih lengkap.</div>

							</li>

						</ul>
					</div>
				</div>

    <div role="main" class="main shop">

      <div class="container">

        <div class="row">
          <div class="col-md-12">
            <hr class="tall">
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <img src="{{asset('/porto/img/tugass.jpg')}}" width="100%">
          </div>
        </div>

      </div>

    </div>

  </div>
@endsection
