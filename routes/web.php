<?php

Route::get('/', function () {
    return view('welcome');
});
Route::get('/object_wisata', 'PendaftaranController@index');
Route::get('/tiket', 'SeleksiController@index');
Route::get('/tiket/pesan', 'SeleksiController@pesan');
Route::get('/rest_area', 'TentangController@index');
